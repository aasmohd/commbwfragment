package com.example.hp.commbwfragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by H.P on 1/14/2018.
 */

public class FragmentA extends Fragment {

    EditText editText;
    Button button;
    Interfacee interfacee;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
       View view= inflater.inflate(R.layout.fragment_a,container,false);
        editText=(EditText)view.findViewById(R.id.edtxt);
        button=(Button)view.findViewById(R.id.btn);





        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        interfacee=(Interfacee)getActivity();// getAcivity is instance of Mainactivity so implement interface in main activity only...

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text= editText.getText().toString();
                interfacee.sendText(text);

            }
        });

    }// here i use onActivityCreated because i need ref of activity for typecasting and also otherwise i need to implement on click listener
    //also for on click
}
